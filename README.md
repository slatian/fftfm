# Fuzzy Find The Fricken Manual

Uses `fzf` as an interactive UI for `apropos` and `man` or with `fftfm --rfc` for RFCs.

## Usage

In man mode (`fftfm`) the first word of the search query is passed to `apropos` as the query, the rest will be handled by `fzf`. You can prepend the query with a section that starts with a number, this will be passed using the `--section` option on apropos.

In RFC mode (`fftfm --rfc`) You can search the description given in the `rfc-index.txt` file ([see below](#rfcs-relevant-downloads)) using `fzf`. If you prepend a number the list only RFCs whose number has the searched for number as prefix will be included in your search.

## Testing

For running without installing the scripts into your `$PATH` use the `./run.sh` script as if it was the `fftfm` command.

## RFCs

By default this script in RFC mode expects to find all RFCs in `/usr/share/doc/rfc/`. Conveniently there is the `rfc` for Arch Linux that installs the expected files there.

If your distribution does not ship with such a package, you can download the RFCs by some other means and pint the `RFC_DOC_DIRECTORY` environment variable at it.

### RFCs - Relevant Downloads
* https://www.rfc-editor.org/rfc/tar/RFC-all.tar.gz - All RFCS conveniently in one package, place the textfiles at `$RFC_DOC_DIRECTORY/txt/`
* https://www.rfc-editor.org/rfc/rfc-index.txt - A File that contains a title and metadata for each RFC in human and machine-readable format. place at `$RFC_DOC_DIRECTORY/rfc-index.txt`.
