#!/bin/sh

# Fuzzy Find The Fricken Manual
# RFC index transformation and search script

# Copyright (C) 2022 Slatian
# License: GPL-3.0-only

RFC_DOC_DIRECTORY="${RFC_DOC_DIRECTORY:-/usr/share/doc/rfc/}"

COMMAND="$1"

show_help() {
cat <<EOF
Usage: fftfm-man <command> [<arg> ...]

COMMANDS

search <query>
	Returns a list of manpages matching the query
highlight
	Colorize the utput of the search command
view-in-terminal <query>
	Take a line output from search or a query and open it for viewing it in the terminal

ENVOIRNMENT

RFC_DOC_DIRECTORY
	The directory containing the RFCs from https://www.rfc-editor.org/rfc/tar/RFC-all.tar.gz and the rfc-index.txt from https://www.rfc-editor.org/rfc/rfc-index.txt
	Default: /usr/share/doc/rfc/
	
EOF
}

case "$COMMAND" in
	search)
		SECTION="$(printf "%s" "$2" | grep -Po '\K^[0-9]*(?=( |$))' )"

		awk 'head_text { match($0, / *(.+)/, i); head_text = head_text " " i[1]} /^[0-9]+ /{ head_text = $0 } /^ *$/{ if (head_text) { print head_text; head_text = null } }' < "$RFC_DOC_DIRECTORY/rfc-index.txt" | grep "^0*$SECTION" || true
		;;
	highlight)
		fftfm-highlight ;;
	view-in-terminal)
		document_id="$( printf "%s" "$2" | sed -e 's/ .*//' -e 's/^0*//' )"
		sed "$(printf 's/\x0c/\\n────────────────────────────────────────────────────────────────────────\\n/')"  "$RFC_DOC_DIRECTORY/txt/rfc$document_id.txt" | less
		;;
	--help)
		show_help ;;
	*)
		show_help
		exit 1 ;;
esac
